# github-oauth

This is a django application to test out social oauth login with github

## Installation

### requirements

- [docker](https://docs.docker.com/engine/install/)
- [docker compose plugin](https://docs.docker.com/compose/install/compose-plugin/)
- [python](https://www.python.org/) \*recommended if using pyright/pylance for local development
- [pyenv](https://github.com/pyenv/pyenv) \*recommended if using pyright/pylance for local development

| Note this project uses the docker compose plugin instead of python package docker-compose

### Build and run project with docker

```bash
make docker
make migrate
```

go to "http://localhost:18000/health-check/" to validate if all systems are working

### Docker compose commands

```bash
make docker # start the docker applications in local
make docker-d # start the docker applications in detached mode (no stout) in local
make docker-build # build the docker images for local
make docker-build-clean # build the docker images for local with no cache
make docker-stop # stop the docker applications in local
```

### Docker exec commands

```bash
make exec # run the bash shel inside the django docker container
```

### Django commands

```bash
make shell # run the python shell in the django project
make migrate # run the django migrate command
```

### Testing

```bash
make test # run all test in project
```

### Coverage

```bash
make coverage # run the coverage commands
make coverage-html # run the coverage commands and create the html reports
```

### Cleaning Commands

```bash
make pycache-clean # remove all files and folder with __pycache__
```

### Setup Oauth with GitHub

#### create github oauth app

follow the documentation https://django-allauth.readthedocs.io/en/latest/providers.html#github

for local setup use the following

homepage url : http://localhost:18000/
callback url: http://localhost:18000/accounts/github/login/callback/

Save the client id and secret key

#### Create admin user

```bash
make migrate
make exec # run the bash shel inside the django docker container
python manage.py createsuperuser
python manage.py collectstatic
```

#### add social application in django admin

login at "http://localhost:18000/admin/"

create a new social app @ social accounts > social application

```
provider: GitHub
name: github
client id: <client id of the github application>
secret: <secret of the github application>
key: github
sites: <add the site with id 1: default example.com>
```

### test login

go to : http://localhost:18000/accounts/github/login
