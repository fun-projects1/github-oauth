#!/usr/bin/env python
import os
import sys
from pathlib import Path
from src.utils.env_variables import EnvironmentVariable as env, import_env_from_file

if __name__ == "__main__":

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.local")

    current_path = Path(__file__).parent.resolve()
    READ_DOT_ENV_FILE = env("DJANGO_READ_DOT_ENV", default="False").boolean
    if READ_DOT_ENV_FILE:
        import_env_from_file(str(current_path / ".envs/.local.env"))

    try:
        from django.core.management import execute_from_command_line
    except ImportError:
        # The above import may fail for some other reason. Ensure that the
        # issue is really that Django is missing to avoid masking other
        # exceptions on Python 2.
        try:
            import django  # noqa
        except ImportError:
            raise ImportError(
                "Couldn't import Django. Are you sure it's installed and "
                "available on your PYTHONPATH environment variable? Did you "
                "forget to activate a virtual environment?"
            )

        raise

    # This allows easy placement of apps within the interior
    # src directory.
    sys.path.append(str(current_path / "src"))

    execute_from_command_line(sys.argv)
