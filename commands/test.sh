#!/bin/bash

set -o errexit
set -o nounset

coverage run -m pytest  -svv --reuse-db --ds=config.settings.test
coverage xml
coverage report
coverage html
