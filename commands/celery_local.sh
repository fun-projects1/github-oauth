#!/bin/bash

set -o errexit
set -o nounset


watchgod celery.__main__.main --args -A config.celery_app worker -l INFO --concurrency=4 -n worker@%h -f /var/log/celeryworker/%n-%i
