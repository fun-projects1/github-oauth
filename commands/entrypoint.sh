#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset

postgres_ready() {
python << END
import sys

import psycopg2
from src.utils.env_variables import EnvironmentVariable as env

try:
    psycopg2.connect(
        dbname=env("POSTGRES_DB").value,
        user=env("POSTGRES_USER").value,
        password=env("POSTGRES_PASSWORD_FILE", no_whitespace_backspace=True).value,
        host=env("POSTGRES_HOST").value,
        port=env("POSTGRES_PORT").int,
    )
except psycopg2.OperationalError:
    sys.exit(-1)
sys.exit(0)

END
}

until postgres_ready; do
  >&2 echo 'Waiting for PostgreSQL to become available...'
  sleep 1
done
>&2 echo 'PostgreSQL is available'

exec "$@"
