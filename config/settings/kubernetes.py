from .base import *  # noqa
from src.utils.env_variables import EnvironmentVariable as env

# GENERAL
# ------------------------------------------------------------------------------
ENVIRONMENT_NAME = env(
    "DJANGO_ENVIRONMENT_NAME", default="environment name not set"
).value
# https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
SECRET_KEY = env("DJANGO_SECRET_KEY").value


# STORAGES
# ------------------------------------------------------------------------------
# https://django-storages.readthedocs.io/en/latest/#installation
INSTALLED_APPS += ["storages"]  # noqa F405
# https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html#settings
AWS_ACCESS_KEY_ID = env("DJANGO_AWS_ACCESS_KEY_ID").value
# https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html#settings
AWS_SECRET_ACCESS_KEY = env("DJANGO_AWS_SECRET_ACCESS_KEY").value

# https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html#settings
AWS_S3_REGION_NAME = env("DJANGO_AWS_S3_REGION_NAME", default="eu-west-1").value

# STATIC
# ------------------------
DJANGO_AWS_STATIC_BUCKET = env("DJANGO_AWS_STATIC_BUCKET").value
STATICFILES_STORAGE = "src.utils.storages.StaticRootS3Boto3Storage"
# MEDIA
# ------------------------------------------------------------------------------
DJANGO_AWS_MEDIA_BUCKET = env("DJANGO_AWS_MEDIA_BUCKET").value
DEFAULT_FILE_STORAGE = "src.utils.storages.MediaRootS3Boto3Storage"


# ADMIN
# ------------------------------------------------------------------------------
# Django Admin URL regex.
ADMIN_URL = env("DJANGO_ADMIN_URL", "no-admin-url").value

# Anymail
# ------------------------------------------------------------------------------
# https://anymail.readthedocs.io/en/stable/installation/#installing-anymail
INSTALLED_APPS += ["anymail"]  # noqa F405
# https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
# https://anymail.readthedocs.io/en/stable/installation/#anymail-settings-reference
# https://anymail.readthedocs.io/en/stable/esps/mailgun/
EMAIL_BACKEND = "anymail.backends.sendgrid.EmailBackend"

ANYMAIL = {
    "SENDGRID_API_KEY": env("SENDGRID_API_KEY").value,
}


# LOGGING
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#logging
# See https://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.

LOGGING = {
    "version": 1,
    "disable_existing_loggers": True,
    "formatters": {
        "verbose": {
            "format": "%(levelname)s %(asctime)s %(module)s "
            "%(process)d %(thread)d %(message)s"
        }
    },
    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "verbose",
        },
        "default": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": "/var/log/django/default.log",
            "maxBytes": 1024 * 1024 * 15,  # 15 MB
            "backupCount": 10,
            "formatter": "verbose",
        },
        "database": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": "/var/log/django/database.log",
            "maxBytes": 1024 * 1024 * 5,  # 15 MB
            "backupCount": 10,
            "formatter": "verbose",
        },
    },
    "root": {"level": "INFO", "handlers": ["default"]},
    "loggers": {
        "django.db.backends": {
            "level": "ERROR",
            "handlers": ["database"],
            "propagate": False,
        },
        # Errors logged by the SDK itself
        "sentry_sdk": {"level": "ERROR", "handlers": ["console"], "propagate": False},
        "django.security.DisallowedHost": {
            "level": "ERROR",
            "handlers": ["default"],
            "propagate": False,
        },
    },
}

# Your stuff...
# ------------------------------------------------------------------------------
