from .base import *  # noqa
from src.utils.env_variables import EnvironmentVariable as env

# GENERAL
# ------------------------------------------------------------------------------
ENVIRONMENT_NAME = "local"
# https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
SECRET_KEY = env(
    "DJANGO_SECRET_KEY",
    default="LAIxGIvPosolFrINaovW2HlU9YCbH0oAC6AoMBaF6gQ5vcYT4dtK4BoIkXKPMNAm",
).value

# EMAIL
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#email-host
EMAIL_HOST = env("EMAIL_HOST", default="mailhog").value
# https://docs.djangoproject.com/en/dev/ref/settings/#email-port
EMAIL_PORT = env("EMAIL_HOST_PORT", default="1025").int

# WhiteNoise
# ------------------------------------------------------------------------------
# http://whitenoise.evans.io/en/latest/django.html#using-whitenoise-in-development
INSTALLED_APPS = ["whitenoise.runserver_nostatic"] + INSTALLED_APPS  # noqa F405
