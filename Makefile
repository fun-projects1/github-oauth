#!make

DJANGO_CONTAINER_NAME=django
COMPOSE_LOCAL=compose.local.yml

RUN_COMMAND = docker exec -it $(DJANGO_CONTAINER_NAME) bash -c

# COVERAGE_CONFIG = --cov --cov-config=.coveragerc

## DOCKER COMPOSE
docker:
	docker compose -f $(COMPOSE_LOCAL) up ${ARGS}
docker-d:
	ARGS=-d make docker
docker-build:
	COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker compose -f $(COMPOSE_LOCAL) build ${ARGS}
docker-build-clean:
	ARGS=--no-cache make docker-build
docker-stop:
	docker compose -f $(COMPOSE_LOCAL) stop ${ARGS}
##---------------

## DOCKER
exec:
	docker exec -it $(DJANGO_CONTAINER_NAME) bash ${ARGS}
##---------------

## DJANGO COMMANDS
command:
	$(RUN_COMMAND) "${COMMAND}"

shell:
	COMMAND="python manage.py shell_plus" make command

migrate:
	COMMAND="python manage.py migrate" make command
##---------------

## COVERAGE
coverage:
	COMMAND="coverage run -m pytest" make command
	COMMAND="coverage report" make command

coverage-html: coverage
	COMMAND="coverage html" make command
##---------------

## CLEANING
pycache-clean:
	find . -type f -name '*.py[co]' -delete -o -type d -name __pycache__ -delete
##---------------

## DATABASE
db-recreate:
	COMMAND="python manage.py recreate_database" make command
##---------------

## TESTING
test:
	COMMAND="pytest ${APP} -svv --reuse-db ${COVERAGE_CONFIG} --ds=config.settings.test" make command

test-create-db:
	COMMAND="pytest ${APP} -svv --create-db ${COVERAGE_CONFIG} --ds=config.settings.test" make command

test-users:
	APP=src/accounts/users make test
test-auth:
	APP=src/accounts/authentication make test
test-accounts:
	APP=src/accounts make test
test-utils:
	APP=src/utils make test
##---------------


