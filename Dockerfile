ARG PYTHON_VERSION=3.11-slim-bullseye

# define an alias for the specfic python version used in this file.
FROM python:${PYTHON_VERSION} as python

ARG APP_HOME=/app

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

RUN mkdir /var/log/django && mkdir /var/log/celeryworker && mkdir -p /app/staticfiles/

# add mime types
COPY ./config/mime.types /etc/mime.types

# Install apt packages
RUN apt-get update && apt-get install --no-install-recommends -y \
  # dependencies for building Python packages
  build-essential \
  # psycopg2 dependencies
  libpq-dev \
  # ps commands
  procps \
  # Translations dependencies
  gettext \
  # cleaning up unused files
  && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
  && rm -rf /var/lib/apt/lists/*

WORKDIR ${APP_HOME}

RUN addgroup --system -gid 1000 django \
    && adduser  -u 1000 --system --ingroup django django

ENV PATH="/home/django/.local/bin:${PATH}"
# make django owner of the WORKDIR directory as well.
RUN chown django:django ${APP_HOME} /var/log/django /var/log/celeryworker /app/staticfiles

USER django

COPY --chown=django:django requirements.txt requirements.txt
RUN pip install --user -r ./requirements.txt

# copy application code to WORKDIR
COPY --chown=django:django . ${APP_HOME}

ENTRYPOINT ["commands/entrypoint.sh"]

CMD ["commands/gunicorn.sh"]
