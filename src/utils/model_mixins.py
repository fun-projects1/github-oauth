from django.db import models


class TimeStampedMixin(models.Model):
    """
    A model mixin that adds timestampes to the base model
    """

    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
