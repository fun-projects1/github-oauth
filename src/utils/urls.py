from django.urls import path
from src.utils.views import HelloWorldView

app_name = "utils"

urlpatterns = [path("hello-world", HelloWorldView.as_view(), name="hello-world")]
