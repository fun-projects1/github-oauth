import os
import json

from typing import Optional, List, Union


class EnvironmentVariable:
    """
    returns the environment value based on the environment name
    on an instance call
    if the environment value is a path to a swarm secret
    it will return the value of that secret instead
    """

    env_file_extension_name = "_FILE"

    def __init__(
        self,
        environment_name: str,
        default: Optional[str] = None,
        no_whitespace_backspace: bool = False,
        raise_exception: bool = False,
    ):

        self.__correct_call_input_or_raise(environment_name, default)
        self.__environment_name = environment_name
        self.__default_value = default
        self.__raise_exception = raise_exception
        self.remove_whitespace_backspace = no_whitespace_backspace

    @staticmethod
    def __correct_call_input_or_raise(
        environment_name: str, default_value: Optional[str] = None
    ):

        name_is_string = isinstance(environment_name, str)
        if not name_is_string:
            raise Exception("The input value should be of type String")

        name_is_empty = len(environment_name) == 0
        if name_is_empty:
            raise Exception("The input value should not be empty")

        default_is_string_or_none = (
            isinstance(default_value, str) or default_value is None
        )
        if not default_is_string_or_none:
            raise Exception("The default value input is not of type string or none")

    @property
    def value(self) -> Union[str, None]:
        return self.__value()

    def __bool__(self) -> bool:
        return self.boolean

    @property
    def boolean(self) -> bool:
        possible_true = ["True", "true", "yes", "1"]
        return self.__value() in possible_true

    @property
    def list(self) -> List[str]:
        value = self.__value()
        if value is not None:
            return value.split(",")
        else:
            return []

    def __int__(self) -> int:
        return self.int

    @property
    def int(self) -> int:
        value = self.__value()
        if value is not None:
            return int(value)
        else:
            return 0

    def __float__(self) -> float:
        return self.float

    @property
    def float(self) -> float:
        value = self.__value()
        if value is not None:
            return float(value)
        else:
            return 0.0

    def json_load(self) -> Union[dict, List, None]:
        value = self.__value()
        if value is None:
            return None
        else:
            return json.loads(value)

    def __value(self) -> Union[str, None]:
        self.__environment_name_find_file()

        if self.__environment_name_is_file_ref():
            env_value = self.__grab_value_from_path()
        else:
            env_value = self.__grab_env_value()

        if self.remove_whitespace_backspace:
            env_value = self.__remove_whitespace_backspace(env_value)

        return env_value

    def __environment_name_find_file(self):
        if not self.__environment_name_is_file_ref():
            potential_env_name = self.__environment_name + self.env_file_extension_name
            if potential_env_name in os.environ.keys():
                self.__environment_name = potential_env_name

    def __environment_name_is_file_ref(self) -> bool:
        return self.env_file_extension_name == self.__environment_name[-5:]

    def __grab_env_value(self) -> Union[str, None]:
        value = os.environ.get(self.__environment_name, self.__default_value)
        if value is None and self.__raise_exception is True:
            raise Exception(
                f"Environment value: {self.__environment_name} was not found"
            )
        return value

    def __grab_value_from_path(self) -> Union[str, None]:
        environment_value = self.__grab_env_value()
        if environment_value:
            f = open(environment_value, "r")
            value = f.read()
            f.close()
            environment_value = value
        return environment_value

    @staticmethod
    def __remove_whitespace_backspace(value: Union[str, None]) -> Union[str, None]:
        if value:
            value = value.strip().replace("\n", "")
        return value


def import_env_from_file(file_path: str):
    with open(file_path, "r") as file:
        for line in list(file.readlines()):
            striped_line = line.strip()
            empty_line = len(striped_line) == 0
            if not empty_line:

                commented_line = "#" == striped_line[0]
                incorrect_line = (
                    "=" not in striped_line or len(striped_line.split("=")) > 2
                )

                if not commented_line and not empty_line and not incorrect_line:
                    key, value = striped_line.split("=")
                    os.environ[key.strip()] = value.strip()
