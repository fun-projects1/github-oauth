from django.urls.base import reverse


class TestHelloWorldView:
    URL = reverse("utils:hello-world")

    def test_endpoint(self, client):
        response = client.get(self.URL)

        assert response.status_code == 200

    def test_response(self, client):
        response = client.get(self.URL)

        assert "message" in response.data.keys()
        assert response.data.get("message") == "hello world"
