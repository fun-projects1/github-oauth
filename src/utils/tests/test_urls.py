from django.urls.base import reverse


def test_urls_hello_world():
    url = reverse("utils:hello-world")
    assert url == "/api/v1/hello-world"
