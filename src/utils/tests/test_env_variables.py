import os
import pytest
from django.conf import settings
from src.utils.env_variables import EnvironmentVariable as env, import_env_from_file


@pytest.fixture(scope="class")
def environment_variable():
    os.environ["TESTING_ENV_VAR"] = "testing_environment_variable"

    yield
    os.environ.pop("TESTING_ENV_VAR")


@pytest.fixture(scope="class")
def second_environment_variable():
    os.environ["TESTING_ENV_VAR_SECOND"] = "testing_environment_variable_two"

    yield
    os.environ.pop("TESTING_ENV_VAR_SECOND")


@pytest.fixture(scope="class")
def backspace_environment_variable():
    os.environ[
        "TESTING_ENV_VAR_BACKSPACE"
    ] = """
    testing_environment_variable_backspace
    """

    yield
    os.environ.pop("TESTING_ENV_VAR_BACKSPACE")


@pytest.fixture(scope="class")
def environment_secret():
    secret_path = f"{settings.ROOT_DIR}/.envs/test_secrets"
    secret_name = "test_secret"
    os.environ["TESTING_ENV_SECRET_FILE"] = f"{secret_path}/{secret_name}"
    if not os.path.exists(secret_path):
        os.makedirs(secret_path)
    f = open(f"{secret_path}/{secret_name}", "w+")
    f.write("supersecret")
    f.close()

    yield
    os.environ.pop("TESTING_ENV_SECRET_FILE")
    os.remove(f"{secret_path}/{secret_name}")


@pytest.fixture(scope="class")
def environment_json_secret():
    secret_path = f"{settings.ROOT_DIR}/.envs/test_secrets"
    secret_name = "test_json_secret"
    os.environ["TESTING_ENV_SECRET_JSON_FILE"] = f"{secret_path}/{secret_name}"
    if not os.path.exists(secret_path):
        os.makedirs(secret_path)
    f = open(f"{secret_path}/{secret_name}", "w+")
    f.write(
        """
{
    "hello":"world"
    }
            """
    )
    f.close()

    yield
    os.environ.pop("TESTING_ENV_SECRET_JSON_FILE")
    os.remove(f"{secret_path}/{secret_name}")


class TestEnvironmentVariable:
    def test_wrong_input_empty_env_name(self):
        with pytest.raises(Exception) as e_info:

            env()

        assert "missing 1 required positional argument" in str(e_info.value)

    def test_wrong_input_empty_str_env_name(self):
        with pytest.raises(Exception) as e_info:

            env("")

        assert "The input value should not be empty" in str(e_info.value)

    @pytest.mark.parametrize(
        "wrong_input",
        [
            ({"key": "value"},),
            (["env_name"],),
            (None,),
            (101,),
        ],
    )
    def test_wrong_input_no_str_env_name(self, wrong_input):
        with pytest.raises(Exception) as e_info:

            env(wrong_input).value

        assert "The input value should be of type String" in str(e_info.value)

    @pytest.mark.parametrize(
        "wrong_input",
        [
            ({"key": "value"},),
            (["env_name"],),
            (101,),
            (None,),
        ],
    )
    def test_wrong_default_input(self, wrong_input):

        with pytest.raises(Exception) as e_info:

            env("no important name", wrong_input).value

        assert "The default value input is not of type string" in str(e_info.value)

    def test_return_correct_env_variable(self, environment_variable):
        value = env("TESTING_ENV_VAR").value
        assert value == "testing_environment_variable"
        assert value == os.environ["TESTING_ENV_VAR"]

    def test_return_correct_env_secret(self, environment_secret):
        value = env("TESTING_ENV_SECRET_FILE").value
        assert value is not os.environ["TESTING_ENV_SECRET_FILE"]
        assert value == "supersecret"

    def test_return_env_non_file_in_name(self, environment_secret):
        value = env("TESTING_ENV_SECRET").value
        assert value is not os.environ["TESTING_ENV_SECRET_FILE"]
        assert value == "supersecret"

    def test_default_value_no_input(self):

        env_value = env("NO_ENV_VALUE").value
        assert env_value is None

    def test_default_value_no_input_raise_exception(self):

        with pytest.raises(Exception) as e_info:

            env("NO_ENV_VALUE", raise_exception=True).value

        assert "NO_ENV_VALUE" in str(e_info.value)
        assert "not found" in str(e_info.value)

    def test_default_value_is_str(self):

        value = env("NO_ENV_VALUE", "default_value").value
        assert value == "default_value"

    def test_multiple_env_values(
        self, environment_variable, second_environment_variable
    ):
        value = env("TESTING_ENV_VAR").value
        assert value == "testing_environment_variable"
        assert value == os.environ["TESTING_ENV_VAR"]

        second_value = env("TESTING_ENV_VAR_SECOND").value
        assert second_value == "testing_environment_variable_two"
        assert second_value == os.environ["TESTING_ENV_VAR_SECOND"]

    def test_backspace_env_values(self, backspace_environment_variable):

        value = env("TESTING_ENV_VAR_BACKSPACE", no_whitespace_backspace=True).value

        assert value == "testing_environment_variable_backspace"
        assert value != os.environ["TESTING_ENV_VAR_BACKSPACE"]

    def test_get_env_variable(
        self,
    ):
        os.environ["TESTING_ENV_VAR"] = "testing_environment_variable"

    @pytest.mark.parametrize(
        "input, expected_bool",
        [
            ("True", True),
            ("true", True),
            ("1", True),
            ("yes", True),
            ("False", False),
            ("false", False),
            ("0", False),
            ("no", False),
            ("blabla", False),
        ],
    )
    def test_bool_value(self, input, expected_bool):
        os.environ["TESTING_ENV_BOOL"] = input
        assert env("TESTING_ENV_BOOL").boolean is expected_bool
        assert bool(env("TESTING_ENV_BOOL")) is expected_bool
        os.environ.pop("TESTING_ENV_BOOL")

    def test_bool_no_found(self):

        env_value = env("NO_ENV_VALUE").boolean
        assert env_value is False

    def test_bool_no_found_raise_exception(self):
        with pytest.raises(Exception) as e_info:

            env("NO_ENV_VALUE", raise_exception=True).boolean

        assert "NO_ENV_VALUE" in str(e_info.value)

    def test_bool_no_found_default(self):

        assert env("NO_ENV_VALUE", "true").boolean is True

    @pytest.mark.parametrize(
        "input, expected_test",
        [
            ("14", 14),
            ("0", 0),
            ("-1", -1),
        ],
    )
    def test_int_value(self, input, expected_test):
        os.environ["TESTING_ENV_INT"] = input
        assert env("TESTING_ENV_INT").int is expected_test
        assert int(env("TESTING_ENV_INT")) is expected_test
        os.environ.pop("TESTING_ENV_INT")

    def test_int_no_found(self):

        env_value = env("NO_ENV_VALUE").int
        assert env_value == 0

    def test_int_no_found_raise_exception(self):
        with pytest.raises(Exception) as e_info:

            env("NO_ENV_VALUE", raise_exception=True).int

        assert "NO_ENV_VALUE" in str(e_info.value)

    def test_int_wrong_default(self):
        with pytest.raises(Exception) as e_info:

            env("NO_ENV_VALUE", "3.14").int

        assert "invalid literal" in str(e_info.value)

    @pytest.mark.parametrize(
        "input, expected_test",
        [
            ("14.24", float("14.24")),
            ("0", 0.0),
            ("-1", -1.0),
        ],
    )
    def test_float_value(self, input, expected_test):
        os.environ["TESTING_ENV_INT"] = input
        assert env("TESTING_ENV_INT").float == expected_test
        assert float(env("TESTING_ENV_INT")) == expected_test
        os.environ.pop("TESTING_ENV_INT")

    def test_float_no_found(self):

        env_value = env("NO_ENV_VALUE").float
        assert env_value == 0.0

    def test_float_no_found_raise_exception(self):
        with pytest.raises(Exception) as e_info:

            env("NO_ENV_VALUE", raise_exception=True).float

        assert "NO_ENV_VALUE" in str(e_info.value)

    def test_float_wrong_default(self):
        with pytest.raises(Exception) as e_info:

            env("NO_ENV_VALUE", "bla").float

        assert "could not convert string to float" in str(e_info.value)

    @pytest.mark.parametrize(
        "input, expected_test",
        [
            ("one_item", ["one_item"]),
            ("one,two,three", ["one", "two", "three"]),
        ],
    )
    def test_list_value(self, input, expected_test):
        os.environ["TESTING_ENV_INT"] = input
        assert env("TESTING_ENV_INT").list == expected_test
        os.environ.pop("TESTING_ENV_INT")

    def test_list_no_found(self):

        env_value = env("NO_ENV_VALUE").list
        assert env_value == []

    def test_list_no_found_raise_exception(self):
        with pytest.raises(Exception) as e_info:

            env("NO_ENV_VALUE", raise_exception=True).list

        assert "NO_ENV_VALUE" in str(e_info.value)

    @pytest.mark.parametrize(
        "input, expected_test",
        [
            ("one_item", ["one_item"]),
            ("one,two,three", ["one", "two", "three"]),
        ],
    )
    def test_list_defaulr_value(self, input, expected_test):
        assert env("NO_ENV_VALUE", default=input).list == expected_test

    def test_json_secret_file(self, environment_json_secret):
        value = env("TESTING_ENV_SECRET_JSON_FILE").json_load()
        assert isinstance(value, dict)
        assert value == {"hello": "world"}


@pytest.fixture(scope="function")
def environment_file():
    file_name = ".use_for_testing.env"
    file = open(file_name, "w+")
    file.writelines(
        [
            "TESTING_ENV_KEY=value_to_test\n",
            "\n",
            "TESTING_TOMANY_EQUALS=first=second\n",
            "#TESTING_COMMENT=ingore\n",
            "TESTING_ENV_KEY_SECOND=second_value_to_test\n",
            "   TESTING_SPACES_STRIP =   no spaces   \n",
        ]
    )
    file.close()

    yield file_name

    for env_key in os.environ.keys():
        if "TESTING" in env_key:
            os.environ.pop(env_key)
    os.remove(file_name)


class TestImportEnvFromFile:
    def test_read_env_file(self, environment_file):
        assert "TESTING_ENV_KEY" not in os.environ.keys()
        import_env_from_file(environment_file)
        assert "TESTING_ENV_KEY" in os.environ.keys()
        assert os.environ["TESTING_ENV_KEY"] == "value_to_test"

    def test_read_env_file_to_many_equals(self, environment_file):
        assert "TESTING_TOMANY_EQUALS" not in os.environ.keys()
        import_env_from_file(environment_file)
        assert "TESTING_TOMANY_EQUALS" not in os.environ.keys()

    def test_read_env_file_ignore_comments(self, environment_file):
        assert "TESTING_COMMENT" not in os.environ.keys()
        import_env_from_file(environment_file)
        assert "TESTING_COMMENT" not in os.environ.keys()

    def test_read_env_file_to_many_spaces(self, environment_file):
        assert "TESTING_SPACES_STRIP" not in os.environ.keys()
        import_env_from_file(environment_file)
        assert "TESTING_SPACES_STRIP" in os.environ.keys()
        assert os.environ["TESTING_SPACES_STRIP"] == "no spaces"

    def test_read_env_file_multi_values(self, environment_file):
        assert "TESTING_ENV_KEY" not in os.environ.keys()
        assert "TESTING_ENV_KEY_SECOND" not in os.environ.keys()
        assert "TESTING_SPACES_STRIP" not in os.environ.keys()
        import_env_from_file(environment_file)
        assert "TESTING_SPACES_STRIP" in os.environ.keys()
        assert "TESTING_ENV_KEY" in os.environ.keys()
        assert "TESTING_ENV_KEY_SECOND" in os.environ.keys()
        assert os.environ["TESTING_ENV_KEY"] == "value_to_test"
        assert os.environ["TESTING_ENV_KEY_SECOND"] == "second_value_to_test"
        assert os.environ["TESTING_SPACES_STRIP"] == "no spaces"
