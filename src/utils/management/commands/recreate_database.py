from django.conf import settings
from django.core.management.base import BaseCommand
from django.core.management import call_command


class Command(BaseCommand):

    help = "drop database an recreate"

    def handle(self, *args, **options):

        if settings.ENVIRONMENT_NAME in ["testing", "local"]:

            self.reset_database()

            self.migrate()

            self.populate_default_data()

            self.populate_fake_data()

        else:
            raise Exception(
                "This command should only be run in the local or testing environment"
            )

    def reset_database(self):
        self.stdout.write("reset the database")
        call_command("reset_db", "-c", "--noinput")

    def migrate(self):
        self.stdout.write("run migrations")
        call_command("migrate")

    def populate_default_data(self):
        self.stdout.write("populate default data")
        call_command("populate_default_data")

    def populate_fake_data(self):
        self.stdout.write("populate fake data")
        call_command("populate_fake_data")
