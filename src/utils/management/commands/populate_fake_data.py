from django.conf import settings
from django.core.management.base import BaseCommand


class Command(BaseCommand):

    help = "Populate the database with fake data for testing and development purpose"

    def handle(self, *args, **options):

        if settings.ENVIRONMENT_NAME in ["testing", "local"]:

            self.stdout.write("Work in progress")

        else:
            raise Exception(
                "This command should only be run in the local or testing environment"
            )
