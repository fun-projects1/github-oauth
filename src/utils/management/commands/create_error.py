from django.core.management.base import BaseCommand


class Command(BaseCommand):

    help = "Populate the database with fake data for testing and development purpose"

    def handle(self, *args, **options):

        division_by_zero = 1 / 0
        print(division_by_zero)
