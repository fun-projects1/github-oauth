from django.db import models


class SupportedCountriesChoices(models.TextChoices):
    ES = "ES", "es"
    NL = "NL", "nl"
