from django.contrib.auth import get_user_model
from django.forms import ModelForm

User = get_user_model()


class UserUpdateForm(ModelForm):
    class Meta:
        model = User
        fields = ["location", "phone_number"]
