from django.urls import path

from src.accounts.users.views import ProfileFormView

app_name = "users"
urlpatterns = [
    path("profile/", ProfileFormView.as_view()),
]
