import uuid
from typing import List

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _

from src.accounts.users.managers import UserManager
from src.utils.model_mixins import TimeStampedMixin


class User(AbstractUser, TimeStampedMixin):
    """
    User model that hold the info for a user to login
    """

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    username = None
    first_name = None
    last_name = None
    email = models.EmailField(_("email address"), unique=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    location = models.CharField(blank=True, max_length=255, null=True)
    phone_number = models.CharField(blank=True, max_length=255, null=True)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    objects = UserManager()

    def __str__(self):
        return self.email

    @property
    def group_list(self) -> List[str]:
        return list(self.groups.values_list("name", flat=True))
