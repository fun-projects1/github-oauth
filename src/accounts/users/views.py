from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import UpdateView

from src.accounts.users.forms import UserUpdateForm

User = get_user_model()


@method_decorator(login_required, name="dispatch")
class ProfileFormView(UpdateView):
    template_name = "profile.html"
    model = User
    form_class = UserUpdateForm
    success_url = "/accounts/profile/"

    def get_object(self):

        return self.request.user

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)
