import factory

from src.accounts.users.factories import UserFactory


class UserFaker(UserFactory):

    email = factory.LazyAttribute(
        lambda a: "{}@fake.com".format(factory.Faker("first_name")).lower()
    )
    is_active = True

    @factory.post_generation
    def password(self, create, extracted):
        if extracted:
            self.set_password(extracted)
        else:
            self.set_password("Qwertyui")


class OauthLoginUserFaker(UserFaker):
    email = "github@faker.com"
