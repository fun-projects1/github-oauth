from datetime import datetime

import pytest

from src.accounts.users.factories import UserFactory
from src.accounts.users.models import User


@pytest.mark.django_db
class TestUserFactory:
    def test_create_user(self):
        user = UserFactory.create(email="testcreatefactory@test.com")

        assert isinstance(user, User)

    def test_default_values(self):
        user = UserFactory.create(email="testdefaultvaluesuserfactory@test.com")

        assert user.username is None
        assert user.is_active is True
        assert user.is_staff is False
        assert user.is_superuser is False
        assert user.password == ""
        assert user.email == "testdefaultvaluesuserfactory@test.com"
        assert isinstance(user.created_at, datetime)
        assert isinstance(user.updated_at, datetime)

    def test_set_password(self):
        test_password = "testpasswordset"
        user = UserFactory.create(
            email="testsetpassworduserfactory@test.com", password=test_password
        )
        assert user.password != test_password
        assert user.check_password(test_password)
