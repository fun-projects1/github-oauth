from datetime import datetime

import pytest
from django.contrib.auth import get_user_model

from src.accounts.users.models import User


@pytest.mark.django_db
class TestUser:
    def test_create_user(self):
        UserModel = get_user_model()

        user = UserModel.objects.create()

        assert isinstance(user, User)

    def test_default_values(self):
        UserModel = get_user_model()

        user = UserModel.objects.create()

        assert user.username is None
        assert user.is_active is True
        assert user.is_staff is False
        assert user.is_superuser is False
        assert user.password == ""
        assert user.email == ""
        assert isinstance(user.created_at, datetime)
        assert isinstance(user.updated_at, datetime)

    def test_delete_user(self):

        UserModel = get_user_model()

        user = UserModel.objects.create()
        user_id = user.id

        user.delete()

        assert UserModel.objects.filter(id=user_id).exists() is False
