import pytest

from datetime import datetime

from src.accounts.users.models import User
from src.accounts.users.fakers import UserFaker


@pytest.mark.django_db
class TestUserFaker:
    def test_create_user(self):
        user = UserFaker.create(email="testcreatefaker@test.com")

        assert isinstance(user, User)

    def test_default_values(self):
        user = UserFaker.create(email="testdefaultvaluesuserfaker@test.com")

        assert user.username is None
        assert user.is_active is True
        assert user.is_staff is False
        assert user.is_superuser is False
        assert isinstance(user.created_at, datetime)
        assert isinstance(user.updated_at, datetime)

    def test_set_password(self):
        test_password = "testpasswordset"
        user = UserFaker.create(
            email="testsetpassworduserfaker@test.com", password=test_password
        )
        assert user.password != test_password
        assert user.check_password(test_password)

    def test_default_password(self):
        default_password = "Qwertyui"
        user = UserFaker.create(email="testsetpassworduserfaker@test.com")
        assert user.password != default_password
        assert user.check_password(default_password)

    def test_email(self):
        email_to_test = "test_email_faker@test.com"
        user = UserFaker.create(email=email_to_test)

        assert user.email == email_to_test

    def test_default_email(self):
        user = UserFaker.create()

        assert "@fake.com" in user.email
