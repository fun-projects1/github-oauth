from datetime import datetime

import pytest
from django.contrib.auth import get_user_model

from src.accounts.users.models import User

UserModel = get_user_model()


@pytest.mark.django_db
class TestCreateUserManager:
    def test_create_user_instance(self):

        user = UserModel.objects.create_user("testmanager@test.com", "testpassword")

        assert isinstance(user, User)

    def test_user_email(self):

        user = UserModel.objects.create_user("testmanager@test.com", "testpassword")

        assert user.email == "testmanager@test.com"

    def test_user_password(self):

        user = UserModel.objects.create_user("testmanager@test.com", "testpassword")

        assert user.check_password("testpassword")

    def test_normalize_user_email(self):

        user = UserModel.objects.create_user("testManager@tEst.com", "testpassword")

        assert user.email == "testManager@test.com"

    def test_empty_password(self):

        user = UserModel.objects.create_user("testManager@tEst.com", "")

        assert user.check_password("")

    def test_default_values(self):

        user = UserModel.objects.create_user("testmanager@test.com", "testpassword")

        assert user.username is None
        assert user.is_active is True
        assert user.is_staff is False
        assert user.is_superuser is False
        assert isinstance(user.created_at, datetime)
        assert isinstance(user.updated_at, datetime)


@pytest.mark.django_db
class TestCreateSuperUserManager:
    def test_create_superuser_instance(self):

        user = UserModel.objects.create_superuser(
            "testmanager@test.com", "testpassword"
        )

        assert isinstance(user, User)

    def test_superuser_email(self):

        user = UserModel.objects.create_superuser(
            "testmanager@test.com", "testpassword"
        )

        assert user.email == "testmanager@test.com"

    def test_superuser_password(self):

        user = UserModel.objects.create_superuser(
            "testmanager@test.com", "testpassword"
        )

        assert user.check_password("testpassword")

    def test_normalize_superuser_email(self):

        user = UserModel.objects.create_superuser(
            "testManager@tEst.com", "testpassword"
        )

        assert user.email == "testManager@test.com"

    def test_empty_password(self):

        user = UserModel.objects.create_superuser("testManager@tEst.com", "")

        assert user.check_password("")

    def test_default_values(self):

        user = UserModel.objects.create_superuser(
            "testmanager@test.com", "testpassword"
        )

        assert user.username is None
        assert user.is_active is True
        assert user.is_staff is True
        assert user.is_superuser is True
        assert isinstance(user.created_at, datetime)
        assert isinstance(user.updated_at, datetime)
