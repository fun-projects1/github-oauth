from rest_framework.permissions import BasePermission


class IsAuthenticated(BasePermission):
    """
    Allows access only to authenticated users.
    """

    def has_permission(self, request, view):
        return request.user.is_authenticated and request.user and request.user.is_active


class IsStaff(BasePermission):
    """
    Allows access only to staff users.
    """

    message = "This endpoint if for staf users only"

    def has_permission(self, request, view):
        if request.user.is_authenticated and request.user and request.user.is_active:
            return "Staff" in request.user.group_list
        else:
            return False
