from allauth.socialaccount.providers.github.views import GitHubOAuth2Adapter
from allauth.socialaccount.providers.oauth2.client import OAuth2Client
from dj_rest_auth.registration.views import SocialLoginView
from rest_framework_simplejwt.views import TokenRefreshView

from src.accounts.authentication.serializers import CustomTokenRefreshSerializer


class CustomTokenRefreshView(TokenRefreshView):
    """
    Refresh token generator view.
    """

    serializer_class = CustomTokenRefreshSerializer


class GitHubLogin(SocialLoginView):
    adapter_class = GitHubOAuth2Adapter
    callback_url = "http://localhost:18000/callback"
    client_class = OAuth2Client
