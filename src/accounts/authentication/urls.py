from django.urls import path
from rest_framework_simplejwt.views import TokenObtainPairView

from src.accounts.authentication.views import CustomTokenRefreshView, GitHubLogin

app_name = "auth"


urlpatterns = [
    path("token/", TokenObtainPairView.as_view(), name="token_obtain_pair"),
    path("token/refresh/", CustomTokenRefreshView.as_view(), name="token_refresh"),
    path("github/callback/", GitHubLogin.as_view(), name="githublogin"),
]
