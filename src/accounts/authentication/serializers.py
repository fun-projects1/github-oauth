from django.contrib.auth import get_user_model
from rest_framework import exceptions
from rest_framework_simplejwt.serializers import TokenRefreshSerializer
from rest_framework_simplejwt.state import token_backend


class CustomTokenRefreshSerializer(TokenRefreshSerializer):
    """
    Inherit from `TokenRefreshSerializer` and touch the database
    before re-issuing a new access and refresh token and ensure that the user
    exists and is active.
    """

    error_msg = "No active account found with the given credentials"

    def validate(self, attrs):
        token_payload = token_backend.decode(attrs["refresh"])

        try:
            user = get_user_model().objects.get(pk=token_payload["user_id"])
        except get_user_model().DoesNotExist:
            raise exceptions.AuthenticationFailed(self.error_msg, "no_account")

        if not user.is_active:
            raise exceptions.AuthenticationFailed(self.error_msg, "no_active_account")

        return super().validate(attrs)
