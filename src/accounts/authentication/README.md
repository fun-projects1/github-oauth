# Module authentication

This module include all logic about authentication through JWT tokens


## generate RSA keys with OpenSSl

```bash
openssl genrsa -out jwt-key 4096
openssl rsa -in jwt-key -pubout > jwt-key.pub
```

