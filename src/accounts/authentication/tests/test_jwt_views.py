import base64
import json

import pytest
from django.urls import reverse

from src.accounts.users.fakers import UserFaker

from datetime import timedelta

from rest_framework_simplejwt.tokens import RefreshToken


def base64url_decode(input):
    if isinstance(input, str):
        input = input.encode("ascii")

    rem = len(input) % 4

    if rem > 0:
        input += b"=" * (4 - rem)

    return base64.urlsafe_b64decode(input)


@pytest.fixture(scope="function")
def jwt_user_data(db):
    print("\nCREATE CLASS FIXTURE : jwt user data")
    post_data = {
        "email": "jlennon@testaccount.com",
        "password": "superSecretPassword",
    }
    user = UserFaker.create(**post_data)
    yield {"post_data": post_data, "user": user}
    print("\nTEARDOWN CLASS FIXTURE : jwt user data")
    user.delete()


@pytest.mark.django_db
class TestObtainTokenPair:

    URL = reverse("auth:token_obtain_pair")

    POST_DATA = {
        "email": "jlennon@testaccount.com",
        "password": "superSecretPassword",
    }

    def test_endpoint_login(self, client, jwt_user_data):

        response = client.post(self.URL, jwt_user_data["post_data"])

        assert response.status_code == 200

    def test_access_token(self, client, jwt_user_data):

        response = client.post(self.URL, jwt_user_data["post_data"])

        assert "access" in response.data.keys()
        acces_code = response.data["access"]
        assert isinstance(acces_code, str)
        assert len(acces_code.split(".")) == 3

    def test_access_token_header(self, client, jwt_user_data):

        response = client.post(self.URL, jwt_user_data["post_data"])

        acces_code = response.data["access"]
        header, _, _ = acces_code.split(".")
        decode_header = json.loads(str(base64url_decode(header), "utf-8"))
        assert "typ" in decode_header.keys()
        assert decode_header["typ"] == "JWT"
        assert "alg" in decode_header.keys()
        assert decode_header["alg"] == "RS256"

    def test_access_token_payload(self, client, jwt_user_data):

        response = client.post(self.URL, jwt_user_data["post_data"])

        acces_code = response.data["access"]
        _, payload, _ = acces_code.split(".")
        decode_payload = json.loads(str(base64url_decode(payload), "utf-8"))
        assert "token_type" in decode_payload.keys()
        assert "exp" in decode_payload.keys()
        assert "jti" in decode_payload.keys()
        assert "user_id" in decode_payload.keys()
        assert isinstance(decode_payload["token_type"], str)
        assert isinstance(decode_payload["exp"], int)
        assert isinstance(decode_payload["jti"], str)
        assert isinstance(decode_payload["user_id"], str)

    def test_refresh_token(self, client, jwt_user_data):

        response = client.post(self.URL, jwt_user_data["post_data"])

        assert "refresh" in response.data
        acces_code = response.data["refresh"]
        assert isinstance(acces_code, str)
        assert len(acces_code.split(".")) == 3

    def test_refresh_token_header(self, client, jwt_user_data):

        response = client.post(self.URL, jwt_user_data["post_data"])

        acces_code = response.data["refresh"]
        header, _, _ = acces_code.split(".")
        decode_header = json.loads(str(base64url_decode(header), "utf-8"))
        assert "typ" in decode_header.keys()
        assert decode_header["typ"] == "JWT"
        assert "alg" in decode_header.keys()
        assert decode_header["alg"] == "RS256"

    def test_refresh_token_payload(self, client, jwt_user_data):

        response = client.post(self.URL, jwt_user_data["post_data"])

        acces_code = response.data["refresh"]
        _, payload, _ = acces_code.split(".")
        decode_payload = json.loads(str(base64url_decode(payload), "utf-8"))
        assert "token_type" in decode_payload.keys()
        assert "exp" in decode_payload.keys()
        assert "jti" in decode_payload.keys()
        assert "user_id" in decode_payload.keys()
        assert isinstance(decode_payload["token_type"], str)
        assert isinstance(decode_payload["exp"], int)
        assert isinstance(decode_payload["jti"], str)

    def test_update_last_login(self, client, jwt_user_data):
        user = jwt_user_data["user"]
        user.last_login = None
        user.save()

        response = client.post(self.URL, jwt_user_data["post_data"])

        assert response.status_code == 200
        user.refresh_from_db()
        assert user.last_login is not None


@pytest.fixture(scope="class")
def refresh_token(django_db_blocker):

    with django_db_blocker.unblock():

        print("\nCREATE FUNCTION FIXTURE : refresh_token")
        user = UserFaker.create(
            email="johnrefresh@testaccount.com",
            password="superSecretPassword",
        )
        refresh = RefreshToken.for_user(user)

        yield {
            "refresh": str(refresh),
            "access": str(refresh.access_token),
        }
        print("\nTEARDOWN FUNCTION FIXTURE : refresh_token")
        user.delete()


@pytest.fixture(scope="function")
def expired_refresh_token(django_db_blocker, settings):

    with django_db_blocker.unblock():

        print("\nCREATE FUNCTION FIXTURE : expired_refresh_token")
        user = UserFaker.create(
            email="johnexprefresh@testaccount.com",
            password="superSecretPassword",
        )

        refresh_token = RefreshToken
        refresh_token.lifetime = timedelta(seconds=0)
        refresh = refresh_token.for_user(user)

        yield {
            "refresh": str(refresh),
            "access": str(refresh.access_token),
        }
        print("\nTEARDOWN FUNCTION FIXTURE : expired_refresh_token")
        user.delete()
        refresh_token.lifetime = settings.SIMPLE_JWT["REFRESH_TOKEN_LIFETIME"]


@pytest.fixture(scope="class")
def non_active_refresh_token(django_db_blocker):

    with django_db_blocker.unblock():

        print("\nCREATE FUNCTION FIXTURE : non_active_refresh_token")
        user = UserFaker.create(
            is_active=False,
            email="johnnonactive@testaccount.com",
            password="superSecretPassword",
        )

        refresh = RefreshToken.for_user(user)

        yield {
            "refresh": str(refresh),
            "access": str(refresh.access_token),
        }
        print("\nTEARDOWN FUNCTION FIXTURE : non_active_refresh_token")
        user.delete()


@pytest.fixture(scope="function")
def no_user_refresh_token(django_db_blocker):

    with django_db_blocker.unblock():

        print("\nCREATE FUNCTION FIXTURE : no_user_refresh_token")
        user = UserFaker.create(
            is_active=False,
            email="johnnonactive@testaccount.com",
            password="superSecretPassword",
        )

        refresh = RefreshToken.for_user(user)
        user.delete()
        yield {
            "refresh": str(refresh),
            "access": str(refresh.access_token),
        }
        print("\nTEARDOWN FUNCTION FIXTURE : no_user_refresh_token")


@pytest.mark.django_db
class TestRefreshTokenPair:

    URL = reverse("auth:token_refresh")

    def test_endpoint_refresh(self, client, refresh_token):

        response = client.post(self.URL, refresh_token)

        assert response.status_code == 200

    def test_endpoint_refresh_return_data(self, client, refresh_token):

        response = client.post(self.URL, refresh_token)

        assert "refresh" in response.data.keys()
        assert "access" in response.data.keys()
        assert response.data["refresh"] != refresh_token["refresh"]
        assert response.data["access"] != refresh_token["access"]

    def test_endpoint_expired_refresh_token(self, client, expired_refresh_token):

        response = client.post(self.URL, expired_refresh_token)
        assert response.status_code == 401

    def test_endpoint_non_active_refresh_token(self, client, non_active_refresh_token):

        response = client.post(self.URL, non_active_refresh_token)
        assert response.status_code == 401

    def test_endpoint_no_user_refresh_token(self, client, no_user_refresh_token):

        response = client.post(self.URL, no_user_refresh_token)
        assert response.status_code == 401
